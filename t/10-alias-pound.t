# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 9;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

is($ctl->run(qw(alias one)),"one.example.net\n","alias one");
my $exp = "two.example.net\ndos.example.net\n";
is($ctl->run(qw(alias two)),$exp,"alias two");

is($ctl->run(qw(alias --host dos.example.net)),$exp,"alias --host");

is($ctl->run(qw(alias --add one uno.example.net en.example.net)),undef,'alias --add');
$exp = <<'EOT'
one.example.net
uno.example.net
en.example.net
EOT
;
is($ctl->run(qw(alias one)),$exp,"alias one");

is($ctl->run(qw(alias --remove one one.example.net)),undef,'alias --remove');
$exp = <<'EOT'
uno.example.net
en.example.net
EOT
;
is($ctl->run(qw(alias one)),$exp,"alias one");

__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
	proxy = pound
[pound]
	check = /bin/true
__CONFIG__
ACL "secure"
	"127.0.0.1"
End
ListenHTTPS
	Address 0.0.0.0
        Port 443
        Cert "/etc/ssl/crt"
	Service "one"
		Host -file "$CONF{hostdir}/one"
		Backend
			Address 127.0.0.1
			Port 8081
		End
	End
	Service "two"
		Host -file "$CONF{hostdir}/two"
		ACL "secure"
		Backend
			Address 127.0.0.1
			Port 8082
		End
	End
End
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
