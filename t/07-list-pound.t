# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 5;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

my $exp = <<'EOT';
one              127.0.0.1:8081           one.example.net
two              127.0.0.1:8082           two.example.net (1 aliases)
EOT
;
is($ctl->run(qw(list)),$exp,"list");

$exp = <<'EOT';
one              127.0.0.1:8081           one.example.net
  backends: 127.0.0.1:8081
  domains: one.example.net
two              127.0.0.1:8082           two.example.net (1 aliases)
  backends: 127.0.0.1:8082
  domains: two.example.net,dos.example.net
  ACL: secure
EOT
;
is($ctl->run(qw(list -l)),$exp,"list -l");

$exp = <<'EOT';
two              127.0.0.1:8082           two.example.net (1 aliases)
  backends: 127.0.0.1:8082
  domains: two.example.net,dos.example.net
  ACL: secure
EOT
;
is($ctl->run(qw(list -l two)),$exp,"list -l two");

__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
	proxy = pound
__CONFIG__
ACL "secure"
	"127.0.0.1"
End
ListenHTTPS
	Address 0.0.0.0
        Port 443
        Cert "/etc/ssl/crt"
	Service "one"
		Host -file "$CONF{hostdir}/one"
		Backend
			Address 127.0.0.1
			Port 8081
		End
	End
	Service "two"
		Host -file "$CONF{hostdir}/two"
		ACL "secure"
		Backend
			Address 127.0.0.1
			Port 8082
		End
	End
End
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
