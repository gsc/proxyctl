package Test::Config;
use parent 'App::Proxyctl::Config';
use strict;
use warnings;
use File::Temp;
use File::Path qw(make_path);
use Carp;

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);
    return $self;
}

sub tempdir {
    my $self = shift;
    unless ($self->{_tempdir}) {
	$self->{_tempdir} = File::Temp->newdir();
    }
    return $self->{_tempdir}
}

sub proxy_get {
    my ($self, $key) = @_;
    return $self->get($self->core->proxy, $key)
}

sub confdir { shift->proxy_get('confdir') }
sub hostdir { shift->proxy_get('hostdir') }
sub conffile { shift->proxy_get('conffile') }

sub haproxy {
    my $self = shift;
    return Test::Config::Section->new('haproxy', $self)
}

sub pound {
    my $self = shift;
    return Test::Config::Section->new('pound', $self)
}

# Override the get method.  This version implements additional handling
# for conffile, confdir and hostdir keywords in [haproxy] and [pound]
# sections.  When required, the check_file method is applied to the former,
# and check_dir to the two latter keywords.
#
# NOTE: It is not necessary if the keywords are actually present in the
# configuration file, because then the appropriate check functions are
# invoked automatically by the Config::Parser mechanics.  It is only necessary
# when the default values for these settings are used.  This is a FIXME
# request for Config::Parser, actually: apply check functions to default
# values as well.  When this is fixed, this method can be removed.
sub get {
    my $self = shift;
    my $ret = $self->SUPER::get(@_);
    if (defined($ret) &&
	$self->tempdir &&
	@_ == 2 &&
	($_[0] eq 'haproxy' || $_[0] eq 'pound')) {
	if ($_[1] eq 'conffile') {
	    $self->check_file(\$ret)
	} elsif ($_[1] eq 'confdir'  || $_[1] eq 'hostdir') {
	    $self->check_dir(\$ret)
	}
    }
    return $ret;
}

sub copy_file {
    my ($self, $in, $filename) = @_;
    open(my $out, '>', $filename)
	or croak "can't create file $filename: $!";
    while (<$in>) {
	s/\$CONF\{\s* ([^\s}]+) \s*\}/ $self->_expand_var($1) /mex;
	print $out $_;
    }
    close $out;
}

sub _expand_var {
    my ($self, $var) = @_;
    return $self->${ \$var };
}

sub restore_paths {
    my ($self, $text) = @_;
    if ($text) {
	my $dir = $self->tempdir;
	my $re = qr/$dir/;
	$text =~ s{$re(?=/)}{}gm;
    }
    return $text
}

# check_dir ensures that the directory name ($$valref) starts with the
# temporary directory name, and modifies it, if it doesn't.  Then it
# creates the directory, if necessary.
sub check_dir {
    my ($self, $valref, $prev_value, $locus) = @_;
    my $val = $$valref;
    unless (rindex($val, $self->tempdir, 0) == 0) {
	$val = File::Spec->catfile($self->tempdir, File::Spec->splitdir($val));
	unless (-d $val || make_path($val, { chmod => 0755 })) {
	    croak "can't create configuration directory $val: $!";
	}
	$$valref = $val;
    }
    return 1
}

# check_file ensures that the file name ($$valref) starts with the
# temporary directory name, and modifies it, if it doesn't.  It checks
# if the directory part of the file name exists and creates it if
# necessary.
#
# It does not check if the file exists: the caller is supposed to
# take care of it.
sub check_file {
    my ($self, $valref, $prev_value, $locus) = @_;
    my $val = $$valref;
    unless (rindex($val, $self->tempdir, 0) == 0) {
	$val = File::Spec->catfile($self->tempdir, File::Spec->splitdir($val));
	$$valref = $val;
    }
    my (undef, $dir, undef) = File::Spec->splitpath($val);
    unless (-d $dir || make_path($dir, { chmod => 0755 })) {
	croak "can't create configuration directory $val: $!";
    }
    return 1;
}

1;

package Test::Config::Section;
use strict;
use warnings;

sub new {
    my ($class, $scope, $config) = @_;
    bless {
	scope => $scope,
	config => $config
    }, $class;
}

sub _get {
    my ($self, $key) = @_;
    return $self->{config}->get($self->{scope}, $key)
}

sub confdir { shift->_get('confdir') }
sub conffile { shift->_get('conffile') }
sub hostdir { shift->_get('hostdir') }

1;
