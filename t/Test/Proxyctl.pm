package Test::Proxyctl;
use strict;
use warnings;
use File::Basename;
use File::Temp;
use Test::Config;
use Carp;
use App::Proxyctl;

my %datafile;

CHECK {
    no strict 'refs';
    local $/;

    my %fh = (
	<main::DATA> =~ m#^__(.+?)__\n(.*?)\n?(?=\n?__|\z)#msg
    );

    for (keys(%fh)) {
	open *{$_}, '<', \$fh{$_} or die "inline open failed: $!";
	$datafile{$_} = \*{$_}
    }
}

use vars qw(*PROXYCTL *CONFIG);

sub new {
    my $class = shift;

    my $config = new Test::Config(
	filename => '__PROXYCTL__',
	fh => \*PROXYCTL,
	line => 0
    );

    # Create proxy configuration file.
    $config->copy_file(\*CONFIG, $config->conffile);

    # Create host files.
    foreach my $key (keys %datafile) {
	if ($key =~ m/^HOST_(.+)/) {
	    $config->copy_file($datafile{$key}, File::Spec->catfile($config->hostdir, $1))
	}
    }

    return bless { _config => $config }, $class;
}

sub rewind {
    my $class = shift;
    foreach my $key (keys %datafile) {
	seek($datafile{$key}, 0, 0);
    }
}

sub config { shift->{_config} }

sub config_extract {
    my ($self, $start, $end, %opts) = @_;
    open(my $fh, '<', $self->config->conffile)
	or die "can't open proxy configuration file: $!";
    my $line;
    if ($opts{trim}) {
	$opts{ltrim} = $opts{rtrim} = 1;
    }
    my @result;
    while (<$fh>) {
	++$line;
	if ($line >= $start) {
	    chomp;
	    s/\s+$// if $opts{rtrim};
	    s/^\s+// if $opts{ltrim};
	    next if /^$/ && $opts{blankelim};
	    push @result, $self->config->restore_paths($_);
	}
	last if defined($end) && $line == $end;
    }
    close($fh);
    return join("\n", @result)."\n"
}

sub run {
    my $self = shift;
    @ARGV = @_;

    local *STDOUT;
    open(STDOUT, '>', \my $stdout);
    App::Proxyctl->command($self->config)->run();
    return $self->config->restore_paths($stdout);
}

1;
