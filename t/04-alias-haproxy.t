# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 9;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

is($ctl->run(qw(alias one)),"one.example.net\n","alias one");
my $exp = "two.example.net\ndos.example.net\n";
is($ctl->run(qw(alias two)),$exp,"alias two");
is($ctl->run(qw(alias --host dos.example.net)),$exp,"alias --host");

is($ctl->run(qw(alias --add one uno.example.net en.example.net)),undef,'alias --add');
$exp = <<'EOT'
one.example.net
uno.example.net
en.example.net
EOT
;
is($ctl->run(qw(alias one)),$exp,"alias one");

is($ctl->run(qw(alias --remove one one.example.net)),undef,'alias --remove');
$exp = <<'EOT'
uno.example.net
en.example.net
EOT
;
is($ctl->run(qw(alias one)),$exp,"alias one");

__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
[haproxy]
        confdir = /etc/haproxy
        conffile = /etc/haproxy/haproxy.cfg
        hostdir = /etc/haproxy/hosts
	check = /bin/true

__CONFIG__
frontend https-in
    mode http
    bind :::443 v4v6 ssl crt /etc/ssl/acme/crt
    acl secure src 127.0.0.1
    acl secure src 127.0.0.2
    use_backend one if { hdr(host) -f $CONF{hostdir}/one }
    use_backend two if { hdr(host) -f $CONF{hostdir}/two }
backend one
    server localhost 127.0.0.1:8081
backend two
    server localhost 127.0.0.1:8082
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
