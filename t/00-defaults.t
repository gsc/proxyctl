# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 3;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

my $exp = <<'EOT'
core.first-port=8080
core.last-port=65353
core.proxy="haproxy"
haproxy.confdir="/etc/haproxy"
haproxy.conffile="/etc/haproxy/haproxy.cfg"
haproxy.hostdir="/etc/haproxy/hosts"
pound.confdir="/etc/pound"
EOT
;

is($ctl->run('vars'),$exp,"defaults");
__DATA__
__PROXYCTL__
__CONFIG__
