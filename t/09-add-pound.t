# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 8;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

is($ctl->run(qw(add three three.example.net)),"127.0.0.1:8083\n","add");

#print $ctl->run(qw(showconfig));

my $exp = <<'EOT';
one              127.0.0.1:8081           one.example.net
two              127.0.0.1:8082           two.example.net (1 aliases)
three            127.0.0.1:8083           three.example.net
EOT
;

is($ctl->run("list"),$exp,"list");

is($ctl->run(qw(add -p 8085 four four.example.net)),"127.0.0.1:8085\n","add -p");

$exp = <<'EOT';
one              127.0.0.1:8081           one.example.net
two              127.0.0.1:8082           two.example.net (1 aliases)
three            127.0.0.1:8083           three.example.net
four             127.0.0.1:8085           four.example.net
EOT
;

is($ctl->run("list"),$exp,"list");

is($ctl->run(qw(add --acl secure five five.example.net)),"127.0.0.1:8086\n","add --acl");

$exp = <<'EOT';
five             127.0.0.1:8086           five.example.net
  backends: 127.0.0.1:8086
  domains: five.example.net
  ACL: secure
EOT
;

is($ctl->run(qw(list -l five)),$exp,"list -l five");
__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
	proxy = pound
[pound]
	check = /bin/true
__CONFIG__
ACL "secure"
	"127.0.0.1"
End
ListenHTTPS
	Address 0.0.0.0
        Port 443
        Cert "/etc/ssl/crt"
	Service "one"
		Host -file "$CONF{hostdir}/one"
		Backend
			Address 127.0.0.1
			Port 8081
		End
	End
	Service "two"
		Host -file "$CONF{hostdir}/two"
		ACL "secure"
		Backend
			Address 127.0.0.1
			Port 8082
		End
	End
End
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
