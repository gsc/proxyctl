# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 7;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

is($ctl->run(qw(drop two)),undef,"drop");

is($ctl->run(qw(list)),
	     "one              127.0.0.1:8081           one.example.net\n",
	     "list");

is($ctl->run(qw(drop one)),undef,"drop");
is($ctl->run(qw(list)),undef,"list");

my $exp = << 'EOT';
ACL "secure"
    "127.0.0.1"
End
ListenHTTPS
    Address 0.0.0.0
    Port 443
    Cert "/etc/ssl/crt"
End
EOT
;
is($ctl->run(qw(showconfig)),$exp,"showconfig")

__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
	proxy = pound
[pound]
	check = /bin/true
__CONFIG__
ACL "secure"
	"127.0.0.1"
End
ListenHTTPS
	Address 0.0.0.0
        Port 443
        Cert "/etc/ssl/crt"
	Service "one"
		Host -file "$CONF{hostdir}/one"
		Backend
			Address 127.0.0.1
			Port 8081
		End
	End
	Service "two"
		Host -file "$CONF{hostdir}/two"
		ACL "secure"
		Backend
			Address 127.0.0.1
			Port 8082
		End
	End
End
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
