# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 4;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

my $exp = <<'EOT';
/etc/pound/pound.cfg:1-3: secure
EOT
;
is($ctl->run("listacl"),$exp,"listacl");

$exp = <<'EOT';
/etc/pound/pound.cfg:1-3: secure
ACL "secure"
	"127.0.0.1"
End

EOT
;

is($ctl->run(qw(listacl -l)),$exp,"listacl -l");

__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
	proxy = pound
[pound]
        confdir = /etc/pound
        conffile = /etc/pound/pound.cfg
        hostdir = /etc/pound/hosts
__CONFIG__
ACL "secure"
	"127.0.0.1"
End
ListenHTTPS
	Address 0.0.0.0
        Port 443
        Cert "/etc/ssl/crt"
	Service "one"
		Host -file "$CONF{hostdir}/one"
		Backend
			Address 127.0.0.1
			Port 8081
		End
	End
	Service "two"
		Host -file "$CONF{hostdir}/two"
		ACL "secure"
		Backend
			Address 127.0.0.1
			Port 8082
		End
	End
End
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
