# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 5;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

my $exp = <<'EOT';
one              127.0.0.1:8081           one.example.net
two              127.0.0.1:8082           two.example.net (1 aliases)
EOT
;
is($ctl->run(qw(list)),$exp,"list");

$exp = <<'EOT';
one              127.0.0.1:8081           one.example.net
  backends: 127.0.0.1:8081
  domains: one.example.net
two              127.0.0.1:8082           two.example.net (1 aliases)
  backends: 127.0.0.1:8082
  domains: two.example.net,dos.example.net
  ACL: secure
EOT
;
is($ctl->run(qw(list -l)),$exp,"list -l");

$exp = <<'EOT';
two              127.0.0.1:8082           two.example.net (1 aliases)
  backends: 127.0.0.1:8082
  domains: two.example.net,dos.example.net
  ACL: secure
EOT
;
is($ctl->run(qw(list -l two)),$exp,"list -l two");

__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
[haproxy]
        confdir = /etc/haproxy
        conffile = /etc/haproxy/haproxy.cfg
        hostdir = /etc/haproxy/hosts

__CONFIG__
frontend https-in
    mode http
    bind :::443 v4v6 ssl crt /etc/ssl/acme/crt
    acl secure src 127.0.0.1
    use_backend one if { hdr(host) -f $CONF{hostdir}/one }
    use_backend two if { hdr(host) -f $CONF{hostdir}/two } secure
backend one
    server localhost 127.0.0.1:8081
backend two
    server localhost 127.0.0.1:8082
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
