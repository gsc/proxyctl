# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 18;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

is($ctl->run(qw(acl one)),undef, "acl one");
is($ctl->run(qw(acl two)), "secure\n", "acl two");
is($ctl->run(qw(acl --list two)), "secure\n", "acl --list two");

is($ctl->run(qw(acl --add two local)),undef, "add acls");
is($ctl->run(qw(acl --list two)), "secure local\n", "acl --list two");
is($ctl->config_extract(15, 15, trim => 1),
  "use_backend two if { hdr(host) -f /etc/haproxy/hosts/two } secure local\n",
   "check source");

is($ctl->run(qw(acl --add one !local)),undef, "acl --add one !local");
is($ctl->config_extract(14, 14, trim => 1),
   "use_backend one if { hdr(host) -f /etc/haproxy/hosts/one } !local\n",
   "check source");

is($ctl->run(qw(acl --delete one !local)),undef, "acl --delete one !local");
is($ctl->config_extract(14, 14, trim => 1),
   "use_backend one if { hdr(host) -f /etc/haproxy/hosts/one }\n",
   "check source");

is($ctl->run(qw(acl --delete two secure)),undef,"acl --delete two secure");
is($ctl->config_extract(15, 15, trim => 1),
   "use_backend two if { hdr(host) -f /etc/haproxy/hosts/two } local\n",
   "check source");

is($ctl->run(qw(acl --add two !secure)),undef, "acl --add two !secure");
is($ctl->config_extract(15, 15, trim => 1),
   "use_backend two if { hdr(host) -f /etc/haproxy/hosts/two } local !secure\n",
   "check source");

is($ctl->run(qw(acl --delete two !secure local)),undef,"acl --delete two !secure local");
is($ctl->config_extract(15, 15, trim => 1),
   "use_backend two if { hdr(host) -f /etc/haproxy/hosts/two }\n",
   "check source");    

__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
[haproxy]
        confdir = /etc/haproxy
        conffile = /etc/haproxy/haproxy.cfg
        hostdir = /etc/haproxy/hosts
	check = /bin/true
__CONFIG__
userlist naivepassword
user admin insecure-password guessME

acl limited src 127.0.0.2
acl limited http_auth(naivepassword)

acl local src 127.0.0.1

frontend https-in
    mode http
    bind :::443 v4v6 ssl crt /etc/ssl/acme/crt
    acl secure src -f /etc/haproxy/secure.acl

    use_backend one if { hdr(host) -f $CONF{hostdir}/one }
    use_backend two if { hdr(host) -f $CONF{hostdir}/two } secure
backend one
    server localhost 127.0.0.1:8081
backend two
    server localhost 127.0.0.1:8082
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
