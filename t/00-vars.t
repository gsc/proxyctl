# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 3;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

my $exp = <<'EOT'
core.first-port=8081
core.hostname="test.example.org"
core.last-port=65353
core.proxy="haproxy"
core.rc=["/bin/true"]
haproxy.confdir="/usr/local/etc/haproxy"
haproxy.conffile="/usr/local/etc/haproxy.cfg"
haproxy.hostdir="/usr/local/etc/haproxy/hosts"
pound.confdir="/etc/pound"
EOT
;

is($ctl->run('vars'),$exp,"defaults");
__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
[haproxy]
        confdir = /usr/local/etc/haproxy
        conffile = /usr/local/etc/haproxy.cfg
        hostdir = /usr/local/etc/haproxy/hosts
__CONFIG__
