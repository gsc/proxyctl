# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 4;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

my $exp = <<'EOT';
/etc/haproxy/haproxy.cfg:5-6: limited
/etc/haproxy/haproxy.cfg:11: secure
EOT
;

is($ctl->run(qw(listacl)),$exp,'listacl');

$exp = <<'EOT'
/etc/haproxy/haproxy.cfg:5-6: limited
acl limited src 127.0.0.2
acl limited http_auth(naivepassword)

/etc/haproxy/haproxy.cfg:11: secure
    acl secure src -f /etc/haproxy/secure.acl

EOT
;

is($ctl->run(qw(listacl -l)),$exp,'listacl -l');

__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
[haproxy]
        confdir = /etc/haproxy
        conffile = /etc/haproxy/haproxy.cfg
        hostdir = /etc/haproxy/hosts
__CONFIG__

userlist naivepassword
user admin insecure-password guessME

acl limited src 127.0.0.2
acl limited http_auth(naivepassword)

frontend https-in
    mode http
    bind :::443 v4v6 ssl crt /etc/ssl/acme/crt
    acl secure src -f /etc/haproxy/secure.acl

    use_backend one if { hdr(host) -f $CONF{hostdir}/one }
    use_backend two if { hdr(host) -f $CONF{hostdir}/two } secure
backend one
    server localhost 127.0.0.1:8081
backend two
    server localhost 127.0.0.1:8082
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
