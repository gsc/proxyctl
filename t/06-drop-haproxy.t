# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 7;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

is($ctl->run(qw(drop two)),undef,"drop");

is($ctl->run(qw(list)),
	     "one              127.0.0.1:8081           one.example.net\n",
	     "list");

is($ctl->run(qw(drop one)),undef,"drop");
is($ctl->run(qw(list)),undef,"list");

my $exp = << 'EOT';
frontend https-in
    mode http
    bind :::443 v4v6 ssl crt /etc/ssl/acme/crt
    acl secure src 127.0.0.1
    acl secure src 127.0.0.2
EOT
;
is($ctl->run(qw(showconfig)),$exp,"showconfig")
__DATA__
__PROXYCTL__
[core]
        hostname = test.example.org
        rc = /bin/true
        first-port = 8081
[haproxy]
        confdir = /etc/haproxy
        conffile = /etc/haproxy/haproxy.cfg
        hostdir = /etc/haproxy/hosts
	check = /bin/true

__CONFIG__
frontend https-in
    mode http
    bind :::443 v4v6 ssl crt /etc/ssl/acme/crt
    acl secure src 127.0.0.1
    acl secure src 127.0.0.2
    use_backend one if { hdr(host) -f $CONF{hostdir}/one }
    use_backend two if { hdr(host) -f $CONF{hostdir}/two }
backend one
    server localhost 127.0.0.1:8081
backend two
    server localhost 127.0.0.1:8082
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
