# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More;

BEGIN {
    plan tests => 18;
    use_ok('Test::Proxyctl');
}

my $ctl = new Test::Proxyctl;
isa_ok($ctl, 'Test::Proxyctl');

is($ctl->run(qw(acl one)),undef, "acl one");
is($ctl->run(qw(acl two)), "secure\n", "acl two");
is($ctl->run(qw(acl --list two)), "secure\n", "acl --list two");

is($ctl->run(qw(acl --add two local)),undef, "add acls");
is($ctl->run(qw(acl --list two)), "secure local\n", "acl --list two");
is($ctl->config_extract(24, 25, trim => 1),
   q{ACL "secure"
ACL "local"
}, "check source");

is($ctl->run(qw(acl --add one !local)),undef, "acl --add one !local");
is($ctl->config_extract(17, 17, trim => 1),q{Not ACL "local"
}, "check source");

is($ctl->run(qw(acl --delete one !local)),undef, "acl --delete one !local");
is($ctl->config_extract(11, 17, trim => 1),
q{Service "one"
Host -file "/etc/pound/hosts/one"
Backend
Address 127.0.0.1
Port 8081
End
End
}, "check source");

is($ctl->run(qw(acl --delete two secure)),undef,"acl --delete two secure");
is($ctl->config_extract(18, 25, trim => 1),
q{Service "two"
Host -file "/etc/pound/hosts/two"
Backend
Address 127.0.0.1
Port 8082
End
ACL "local"
End
},
"check source");

is($ctl->run(qw(acl --add two !secure)),undef, "acl --add two !secure");
is($ctl->config_extract(18, 26, trim => 1),
q{Service "two"
Host -file "/etc/pound/hosts/two"
Backend
Address 127.0.0.1
Port 8082
End
ACL "local"
Not ACL "secure"
End
},
"check source");

is($ctl->run(qw(acl --delete two !secure local)),undef,"acl --delete two !secure local");
is($ctl->config_extract(18, 24, trim => 1),
q{Service "two"
Host -file "/etc/pound/hosts/two"
Backend
Address 127.0.0.1
Port 8082
End
End
},
"check source");

__DATA__
__PROXYCTL__
[core]
	hostname = test.example.org
	rc = /bin/true
	first-port = 8081
	proxy = pound
[pound]
	check = /bin/true
__CONFIG__
ACL "secure"
	"192.0.2.1"
End
ACL "local"
	"127.0.0.0/8"
End
ListenHTTPS
	Address 0.0.0.0
	Port 443
	Cert "/etc/ssl/crt"
	Service "one"
		Host -file "$CONF{hostdir}/one"
		Backend
			Address 127.0.0.1
			Port 8081
		End
	End
	Service "two"
		Host -file "$CONF{hostdir}/two"
		Backend
			Address 127.0.0.1
			Port 8082
		End
		ACL "secure"
	End
End
__HOST_one__
one.example.net
__HOST_two__
two.example.net
dos.example.net
