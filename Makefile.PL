use strict;
use warnings;
use ExtUtils::MakeMaker;

WriteMakefile(
    NAME              => 'proxyctl',
    VERSION_FROM      => 'lib/App/Proxyctl.pm',
    ABSTRACT_FROM     => 'lib/App/Proxyctl.pm',
    LICENSE           => 'gpl_3',
    AUTHOR            => 'Sergey Poznyakoff <gray@gnu.org>',
    MIN_PERL_VERSION  => 5.006,
    EXE_FILES         => [ 'proxyctl' ],
    MAN1PODS          => {},
    DEFINE            => '-DCTLGROUP=\"$(CTLGROUP)\"',
    macro             => { 'USRGROUP' => 'proxyusr',
			   'CTLGROUP' => 'proxyctl' },
    PREREQ_PM         => {
	'Carp'            => 0,
        'Clone'           => 0,
	'File::Basename'  => 0,
        'File::Spec'      => 0,
        'File::Temp'      => 0,
        'File::stat'      => 0,
        'Getopt::Long'    => 0,
        'Pod::Find'       => 0,
        'Pod::Man'        => 0,
        'Pod::Usage'      => 0,
        'Scalar::Util'    => 0,
        'Text::ParseWords' => 0,
	'Net::DNS'        => 0,
	'Sys::Hostname'   => 0,
	'Config::Parser'  => 1.05,
	'Taint::Util'     => 0.08,
	'Config::Proxy'   => 0
     },
 );

package MY;
use Config;
use Pod::Find qw(pod_where);
use Pod::Usage;

sub dynamic_bs { '' }
sub dynamic_lib { '' }
sub dynamic { '' }

sub pod_command_name {
    my ($pack) = @_;
    my %args;
    
    my $msg = "";

    open my $fd, '>', \$msg;

    $args{-input} = pod_where({-inc => 1}, $pack);
    pod2usage(-verbose => 99,
	      -sections => 'NAME',
	      -output => $fd,
	      -exitval => 'NOEXIT',
	      %args);
    my @a = split /\n/, $msg;
    return undef if $#a < 1;
    $msg = $a[1];
    $msg =~ s/^\s+//;
    $msg =~ s/ - .*$//;
    return $msg;
}

sub command_manpages {
    my $s = <<'_MAKE_'
all:: subcommand-manpages
.PHONY: subcommand-manpages
subcommand-manpages: manifypods
	$(NOECHO) $(ECHO) Generating manpages for the subcommands
	$(NOECHO) $(FULLPERLRUN) -pe 's/App::Proxyctl\s+$(MAN3EXT)/proxyctl $(MAN1EXT)/g;s/App::Proxyctl::Command::/proxyctl /g' $(INST_MAN3DIR)/App::Proxyctl.$(MAN3EXT) > $(INST_MAN1DIR)/proxyctl.$(MAN1EXT)
_MAKE_
;
    unshift @INC, 'lib';
    foreach my $file (glob("lib/App/Proxyctl/Command/*.pm")) {
	my $mod = $file;
	$mod =~ s{^.*lib/}{};
	$mod =~ s{\.pm$}{};
	$mod =~ s{/}{::}g;
	if (my $command = pod_command_name($mod)) {
	    my $man1base = $command;
	    $man1base =~ s/ /-/;
	    my $man1file = $man1base . '.$(MAN1EXT)';
	    $s .= "\t\$(NOECHO)\$(FULLPERLRUN) -pe 's/$mod\\s+\$(MAN3EXT)/$man1base \$(MAN1EXT)/' \$(INST_MAN3DIR)/$mod.\$(MAN3EXT) > \$(INST_MAN1DIR)/$man1file\n"
	}
    }
    shift @INC; 
    $s .= "\n";
    return $s;
}

sub postamble {
    my $self = shift;
    my @ret = ( $self->SUPER::postamble,
		'',
		'check: test',
		'',
		'LDFLAGS=`perl -MExtUtils::Embed -e ldopts`',
		'CCFLAGS=`perl -MExtUtils::Embed -e ccopts`',
                'proxyctl: proxyctl.o',
                "\tcc -o proxyctl proxyctl.o \$(LDFLAGS) -lcap" );
    foreach my $inst (qw(perl site vendor)) {
	my $INST = uc $inst;
	push @ret,
	     '',
	     "pure_${inst}_install ::",
	     "\tchown root:\$(USRGROUP) \$(DESTINSTALL${INST}BIN)/proxyctl",
	     "\tchmod u+s \$(DESTINSTALL${INST}BIN)/proxyctl";
    }

    return join("\n", @ret) . "\n" . command_manpages;
}

sub install {
    my $self = shift;
    use constant { ST_INI => 0, ST_RUL => 1, ST_FIN => 2 };
    my $state = ST_INI;
    join "\n",
    map {
	if ($state == ST_INI) {
	    if (/^install/) {
		$state = ST_RUL;
	    }
	} elsif ($state == ST_RUL) {
	    if (/^$/) {
		$_ = "\t\n";
		$state = ST_FIN
	    }
	}
	$_
    } split /\n/, $self->SUPER::install;
}

1;

