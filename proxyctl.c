#include <EXTERN.h>
#include <perl.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <sys/prctl.h>
#include <sys/capability.h>

#ifndef CTLGROUP
# define CTLGROUP "proxyctl"
#endif

static PerlInterpreter *my_perl;

char const script[] = "\
use strict;\n\
use warnings;\n\
use App::Proxyctl;\n\
sub proxyctl {\n\
    $0 = shift;\n\
    @ARGV = @_;\n\
    App::Proxyctl->new($ARGV[0])->run;\n\
}\n\
";

EXTERN_C void boot_DynaLoader (pTHX_ CV* cv);

EXTERN_C void
xs_init(pTHX)
{
	char *file = __FILE__;
	/* DynaLoader is a special case */
	newXS("DynaLoader::boot_DynaLoader", boot_DynaLoader, file);
}

static char const *progname;

static void
abend(int err, char const *fmt, ...)
{
	va_list ap;

	fprintf(stderr, "%s: ", progname);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	if (err)
		fprintf(stderr, ": %s", strerror(err));
	fputc('\n', stderr);
	exit(1);
}

static void
drop_privs(void)
{
	struct group *gr;
	struct passwd *pw;

	pw = getpwuid(getuid());
	if (!pw)
		abend(0, "who are you?");

	gr = getgrnam(CTLGROUP);
	if (!gr)
		abend(errno, "%s: no such group", CTLGROUP);

	if (setegid(gr->gr_gid))
		abend(errno, "setegid");

	if (setgid(gr->gr_gid))
		abend(errno, "setgid");

	if (cap_set_proc(cap_from_text("cap_chown=ep")))
		abend(errno, "cap_set_proc");

	if (prctl(PR_SET_KEEPCAPS, 1))
		abend(errno, "prctl");

	if (setuid(getuid()))
		abend(errno, "setuid");

	if (cap_set_proc(cap_from_text("cap_chown+ep")))
		abend(errno, "cap_set_proc cap_chown+ep");
}

int
main(int argc, char **argv)
{
	char *args[] = { "", "-e", "0", NULL };

	progname = argv[0];
	drop_privs();
	umask(002);

	my_perl = perl_alloc();
	perl_construct(my_perl);

	perl_parse(my_perl, xs_init, 3, args, NULL);

	/*** skipping perl_run() ***/
	eval_pv(script, TRUE);
	call_argv("proxyctl", G_DISCARD, argv);

	perl_destruct(my_perl);
	perl_free(my_perl);
	return 0;
}
