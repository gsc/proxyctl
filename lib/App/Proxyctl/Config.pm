package App::Proxyctl::Config;
use parent 'Config::Parser::Ini';
use strict;
use warnings;
use Carp;
use File::Spec;
use Data::Dumper;
use Config::AST qw(:sort);

sub mangle {
    my $self = shift;
    my $proxy = $self->core->proxy;

    my $confdir = $self->get($proxy. 'confdir');
    unless ($confdir) {
	$confdir = '/etc/' . $proxy;
    }

    unless ($self->get($proxy, 'conffile')) {
	$self->set($proxy, 'conffile',
		   File::Spec->catfile($confdir, $proxy . '.cfg'))
    }
    unless ($self->get($proxy, 'hostdir')) {
	$self->set($proxy, 'hostdir',
		   File::Spec->catfile($confdir, 'hosts'));
    }
}

sub check_dir {
    my ($self, $valref, $prev_value, $locus) = @_;
    my $val = $$valref;
    unless (-d $val) {
	$self->error("$val: directory does not exist", locus => $locus);
	return 0;
    }
    return 1;
}

sub check_file {
    my ($self, $valref, $prev_value, $locus) = @_;
    my $val = $$valref;
    unless (-e $val) {
	$self->error("$val: file does not exist", locus => $locus);
	return 0;
    }
    # FIXME: more tests
    return 1;
}

sub check_conffile {
    my $self = shift;
    $self->check_file(@_);
}

1;
__DATA__
[core]
	hostname = STRING :re=.*
	rc = STRING :array :re=.*
	first-port = NUMBER 8080
	last-port = NUMBER 65353
	proxy = STRING :re="^(haproxy)|(pound)" :default haproxy
[haproxy]
	confdir = STRING :check=check_dir :mandatory :default /etc/haproxy
	conffile = STRING :check=check_conffile
	hostdir = STRING :check=check_dir
	check = STRING
[pound]
	confdir = STRING :check=check_dir :mandatory :default /etc/pound
	conffile = STRING :check=check_conffile
	hostdir = STRING :check=check_dir
	check = STRING
[nsupdate ANY]
	ns = STRING :array
	keyfile = STRING :check=check_file
	zone = STRING
