package App::Proxyctl::NSUpdate;
use strict;
use warnings;
use Net::DNS;
use Sys::Hostname;
use Taint::Util;
use Carp;
use Socket;

use constant {
    UPDATE_UNKNOWN => 0,
    UPDATE_SUCCESS => 1,
    UPDATE_ERROR => 2
};

# $zone
# params:
#   target    - cname target
#   ns        - name servers
#   keyfile   - name of the keyfile
#   ttl       = ttl

sub new {
    my $class = shift;
    my $zone = shift;
    my $reporter = shift;
    local %_ = @_;
    
    my $resolver = new Net::DNS::Resolver;
    my @servers;
	
    if (defined $_{ns}) {
	push @servers, @{$_{ns}};
    } else {
	my $query = $resolver->query($zone, "SOA");
	if ($query) {
	    my @rr = $query->answer;
	    untaint($rr[0]->mname);
	    push @servers, $rr[0]->mname;
	} else {
	    croak "can't get soa for $zone: " . $resolver->errorstring;
	}
    }

    my $target = $_{target} // hostname;

    $resolver->nameservers(map {
	if (my @addrs = gethostbyname($_)) {
	    untaint(my @ret = map { inet_ntoa($_) } @addrs[4 .. $#addrs]);
	    @ret
	} else {
	    untaint($_);
	    $_
	}
	} @servers);
    bless {
	zone => $zone,
	reporter => $reporter,
	target => $target,
	resolver => $resolver,
	keyfile => $_{keyfile},
	ttl => $_{ttl} // 600,
	status => UPDATE_UNKNOWN,
    }, $class
}

sub zone { shift->{zone} }
sub target { shift->{target} }
sub resolver { shift->{resolver} }
sub keyfile { shift->{keyfile} }
sub ttl { shift->{ttl} }
sub status {
    my $self = shift;
    if (@_) {
	$self->{status} = shift;
    }
    return $self->{status};
}

sub success { shift->status == UPDATE_SUCCESS }

sub debug {
    my $self = shift;
    return $self->{reporter}->debug(@_);
}

sub error {
    my $self = shift;
    return $self->{reporter}->error(@_);
}

sub clearerr {
    my $self = shift;
    $self->status(UPDATE_UNKNOWN);
}
	
my %nsupdate_diag = (
    NXRRSET => sub {
	my $rr = shift;
	return $rr->type ne 'ANY';
    },
    NXDOMAIN => sub {
	my $rr = shift;
	return $rr->type eq 'ANY';
    }
);    

sub nsupdate_strerror {
    my ($self, $reply, $prereq) = @_;
    my $s;
    if ($prereq and exists($nsupdate_diag{$reply->header->rcode})) {
	if (ref($prereq) ne 'ARRAY') {
	    $prereq = [ $prereq ];
	}
	my $diag = $nsupdate_diag{$reply->header->rcode};
	foreach my $rr (@{$prereq}) {
	    if (&{$diag}($rr)) {
		return "prerequisite \"".$rr->plain ."\" not met";
	    }
	}
    }
    return $reply->header->rcode;
}

sub update {
    my $self = shift;
    my $name = shift;
    local %_ = @_;
    my %ignorerr;

    $self->clearerr;
    my $update = new Net::DNS::Update($self->zone);

    while (my ($k, $v) = each %_) {
	if ($k eq 'ignore') {
	    $ignorerr{$v} = 1;
	} elsif (ref($v) eq 'ARRAY') {
	    foreach my $r (@{$v}) {
		$update->push($k => $r);
	    }
        } else {
	    $update->push($k => $v);
        }
    }

    if ($self->keyfile) {
	$update->sign_tsig($self->keyfile);
    }

    my $reply = $self->resolver->send($update);
    if ($reply) {
	if ($reply->header->rcode eq 'NOERROR') {
	    $self->debug(1, "update of $name successful");
	    $self->status(UPDATE_SUCCESS);
	} elsif ($ignorerr{$reply->header->rcode}) {
	    $self->debug(1, "ignoring: " .
			  $self->nsupdate_strerror($reply, $_{prereq}));
	} else {
	    $self->error("updating $name failed: " .
			 $self->nsupdate_strerror($reply, $_{prereq}));
	    $self->status(UPDATE_ERROR);
	}
    } else {
	$self->error("updating $name failed: " . $self->resolver->errorstring);
	$self->status(UPDATE_ERROR);
    }
    return $self->success;
}

sub register {
    my ($self, $name) = @_;
    $name = lc $name;
    my $query = $self->resolver->query($name, 'CNAME');
    if ($query) {
	foreach my $cname (map { lc($_->rdatastr) =~ /^(.*?)\.?$/ }
                           grep { $_->type eq 'CNAME' } $query->answer) {
	    if ($cname ne $self->target) {
		$self->error("$name points to another host: $cname"
			     . "; my name is ".$self->target);
		$self->status(UPDATE_ERROR);
		return 0;
	    }
	}
    } elsif ($self->resolver->errorstring ne "NXDOMAIN"
	     && $self->resolver->errorstring ne "NOERROR") {
	$self->error($self->resolver->errorstring);
	$self->status(UPDATE_ERROR);
	return 0;
    }
    
    $self->update(
	$name, 
	update => rr_add("$name " . $self->ttl . ' CNAME ' . $self->target)
    );
}

sub deregister {
    my ($self, $name) = @_;
    $self->update(
	$name, 
	prereq => [yxdomain($name),
		   yxrrset("$name CNAME " . $self->target)],
	update => rr_del("$name CNAME"),
	ignore => 'NXDOMAIN'
    )
}

1;
