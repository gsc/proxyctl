package App::Proxyctl::VirtualHost::haproxy;
use strict;
use warnings;
use parent 'App::Proxyctl::VirtualHost::Base';
use Carp;
use File::Spec;
use Config::Proxy::Node::Empty;

#
# Virtual Host Set
#
sub frontend {
    my ($class, $config) = @_;
    my ($frontend) = $config->select(name => 'frontend',
				     arg => { n => 0, v => 'https-in' });
    return $frontend;
}

sub from_backend {
    my ($class, $node, $hostdir) = @_;
    my $self = $class->SUPER::new(find_file($node, $hostdir));
    $self->node($node);
    my ($backend) = $node->root->select(name => 'backend',
					arg => { n => 0,
						 v => $node->arg(0) });
    $self->backend($backend);
    return $self
}

sub virtual_hosts {
    my ($class, $cmd) = @_;
    [map {
	my $be = $class->from_backend($_, $cmd->hostdir);
	$be->valid ? $be : ()
    } $class->frontend($cmd->config)->select(name => 'use_backend')]
}

sub check_name {
    my ($class, $name, $config) = @_;

    my ($be) = $config->select(name => 'backend',
			       arg => { n => 0, v => $name });
    if ($be) {
	return "desired name conflicts with an existing backend ("
	       . $be->locus . ")";
    }
}

sub acl_list {
    my ($class, $config) = @_;
    my %set;
    foreach my $acl ($config->select(name => 'acl'),
		     $class->frontend($config)->select(name => 'acl')) {
	if (exists($set{$acl->arg(0)})) {
	    $set{$acl->arg(0)}->add($acl)
	} else {
	    $set{$acl->arg(0)} = App::Proxyctl::VirtualHost::haproxy::acl->new($acl);
	}
    }
    return \%set
}

#
# Virtual host instance
#

sub create {
    my ($class, $cmd, $name, $port, %opt) = @_;
    my $self = $class->SUPER::new(File::Spec->catfile($cmd->hostdir, $name));
    unlink $self->file;
    if (my $domains = $opt{domains}) {
	$self->add_domain(@$domains);
    }

    my @argv = ( $name, qw/if { hdr(host) -f/, $self->file, '}' );
    if (my $cond = $opt{condition}) {
	push @argv, $cond
    }

    my $node = new Config::Proxy::Node::Statement(
	    kw => 'use_backend',
	    argv => \@argv);
    $self->frontend($cmd->config)->append_node_nonempty($node);
    $self->node($node);

    $node = new Config::Proxy::Node::Section(
	    kw => 'backend',
	    argv => [ $name ]);
    $node->append_node(new Config::Proxy::Node::Statement(
			       kw => 'server',
			       argv => [
				   'localhost',
				   '127.0.0.1:'.$port
			       ]));
    $self->backend($node);
    unless ($cmd->config->tree->ends_in_empty) {
	$cmd->config->tree->append_node(new Config::Proxy::Node::Empty);
    }
    $cmd->config->tree->append_node($node);
    $cmd->config->tree->mark_dirty;
    $self->mark_dirty;

    return $self
}

sub valid {
    my $self = shift;
    return $self->file && $self->backend
}

sub backend {
    my $self = shift;
    if (@_) {
	$self->{_backend} = shift;
    }
    return $self->{_backend};
}

sub node {
    my $self = shift;
    if (@_) {
	$self->{_node} = shift;
    }
    return $self->{_node}
}

sub drop {
    my $self = shift;
    $self->node->drop;
    $self->backend->drop;
    $self->{_domains} = [];
    $self->mark_dirty;
}

sub locus {
    my $self = shift;
    return $self->node->locus
	. " and "
	. $self->backend->locus;
}

sub find_file {
    my ($node, $hostdir) = @_;
    my $rx = qr($hostdir);
    my @argv = $node->argv;
    while (my $arg = shift @argv) {
	if ($arg eq '-f' && -f $argv[0]) {
	    if ($argv[0] =~ s{^($rx/.+)$}{$1}) {
		# Untaint the value
		return $1;
	    } else {
		last;
	    }
	}
    }
}

sub name {
    my $self = shift;
    return $self->node->arg(0);
}

sub acl {
    my $self = shift;
    my @args = $self->node->argv;
    if (join(' ', (@args[1..4],'file',$args[6])) eq 'if { hdr(host) -f file }') {
	return @args[7 .. $#args];
    }
}

sub set_acl {
    my $self = shift;
    my @args = $self->node->argv;
    if (join(' ', (@args[1..4],'file',$args[6])) eq 'if { hdr(host) -f file }') {
	$self->node->argv([@args, @_]);
    }
    $self->node->root->mark_dirty;
}

sub del_acl {
    my $self = shift;
    my @args = $self->node->argv;
    if (join(' ', (@args[1..4],'file',$args[6])) eq 'if { hdr(host) -f file }') {
	my %h;
	@h{@_} = (1) x @_;
	$self->node->argv([@args[0 .. 6],
			   grep { ! $h{$_} } @args[7 .. $#args]]);
}
    $self->node->root->mark_dirty;
}

sub servers {
    my $self = shift;
    croak "can't use this method on invalid backend" unless $self->backend;
    my @ret = map { $_->arg(1) } $self->backend->select(name => 'server');
    if (@_) {
	my $n = shift;
	return undef unless $n < @ret;
	return $ret[$n];
    }
    @ret;
}

1;

package App::Proxyctl::VirtualHost::haproxy::acl;

sub new {
    my ($class, $acl) = @_;
    return bless {
	acl => [ $acl ]
    }
}

sub add {
    my ($self, $acl) = @_;
    push @{$self->{acl}}, $acl;
}

sub locus {
    my $self = shift;
    my $locus = new Text::Locus;
    foreach my $acl (@{$self->{acl}}) {
	$locus->union($acl->locus)
    }
    return $locus;
}

sub write {
    my $self = shift;
    foreach my $acl (@{$self->{acl}}) {
	$acl->write(@_)
    }
}

1;
