package App::Proxyctl::VirtualHost::pound;
use strict;
use warnings;
use parent 'App::Proxyctl::VirtualHost::Base';
use Carp;
use File::Spec;
use Config::Proxy::Node::Empty;

#
# Virtual Host Set
#
sub frontend {
    my ($self, $config) = @_;
    my ($frontend) = $config->select(name => 'ListenHTTPS');
    return $frontend;
}

sub from_service {
    my ($parent, $service, $filename) = @_;
    my $self = $parent->SUPER::new($filename);
    $self->{_service} = $service;
    return $self
}

sub virtual_hosts {
    my ($self, $cmd) = @_;
    [map {
	my @hostnodes = $_->select(name => 'Host',
				   code => sub {
				       my ($node) = @_;
				       return $node->argv() == 2 &&
					      $node->arg(0) eq '-file';
				   });
	my @backends = $_->select(name => 'Backend',
				  code => sub {
				      my ($node) = @_;
				      $node->select(name => 'Port') == 1
				  });
	my $res;

	if (@hostnodes == 1 && @backends == 1) {
	    my $filename = $cmd->config->dequote($hostnodes[0]->arg(1));
	    if (rindex($filename, $cmd->hostdir, 0) == 0 && -f $filename) {
		$res = $self->from_service($_, $filename)
	    }
	}

	if (defined($res)) {
	    $res
	} else {
	    ()
	}
     } $self->frontend($cmd->config)->select(name => 'Service') ]
}

sub check_name {
    my ($self, $name, $config) = @_;
    return undef;
}

sub acl_list {
    my ($class, $config) = @_;
    return {map {
	dequote($_->arg(0)) => $_
    } $config->select(name => 'ACL')}
}

#
# Virtual host instance
#
sub create {
    my ($class, $cmd, $name, $port, %opt) = @_;
    my $self = $class->SUPER::new(File::Spec->catfile($cmd->hostdir, $name));
    unlink $self->file;
    if (my $domains = $opt{domains}) {
	$self->add_domain(@$domains);
    }

    my $service = new Config::Proxy::Node::Section(
	kw => 'Service',
	argv => [ "\"$name\"" ]
    );
    $self->{_service} = $service;
    my $node = new Config::Proxy::Node::Statement(
	kw => 'Host',
	argv => [ '-file', '"'.$self->file.'"' ]
    );

    $service->append_node($node);
    if (my $acl = $opt{condition}) {
	$service->append_node(
	    new Config::Proxy::Node::Statement(kw => 'ACL',
					       argv => [ "\"$acl\"" ])
	)
    }
    my $backend = new Config::Proxy::Node::Section(kw => 'Backend');
    $service->append_node($backend);
    $backend->append_node(
	new Config::Proxy::Node::Statement(kw => 'Address',
					   argv => [ '127.0.0.1' ])
    );
    $backend->append_node(
	new Config::Proxy::Node::Statement(kw => 'Port',
					   argv => [ $port ])
    );
    $backend->append_node(
	new Config::Proxy::Node::Statement(kw => 'End')
    );

    $service->append_node(
	new Config::Proxy::Node::Statement(kw => 'End')
    );

    unless ($cmd->config->tree->ends_in_empty) {
	$cmd->config->tree->append_node(new Config::Proxy::Node::Empty);
    }
    $cmd->frontend->append_node($service);
    $cmd->config->tree->mark_dirty;
    $self->mark_dirty;

    return $self
}

sub service { shift->{_service} }

sub acl_nodes {
    my $self = shift;
    ( $self->service->select(name => 'ACL'),
      $self->service->select(name => 'Not',
			     code => sub {
				 my ($node) = @_;
				 if ($node->tree == 1) {
				     my $s = $node->tree(0);
				     return (
					 $s->is_statement &&
					 lc($s->kw) eq 'acl');
				 }
			     }) )
}

sub acl_name {
    my ($node, $name_only) = @_;
    if (lc($node->kw) eq 'acl') {
	dequote($node->arg(0))
    } else {
	"!".dequote($node->tree(0)->arg(0))
    }
}

sub acl {
    my $self = shift;
    map {
	acl_name($_)
    } $self->acl_nodes;
}

sub set_acl {
    my $self = shift;
    foreach my $acl (@_) {
	if ($acl =~ s/^!//) {
	    my $notnode = new Config::Proxy::Node::Section(kw => 'Not');
	    $notnode->append_node(
		new Config::Proxy::Node::Statement(kw => 'ACL',
						   argv => [ "\"$acl\"" ])
	    );
	    $self->service->append_node($notnode)
	} else {
	    $self->service->append_node(
		new Config::Proxy::Node::Statement(kw => 'ACL',
						   argv => [ "\"$acl\"" ])
	    )
	}
    }
    $self->service->root->mark_dirty;
}

sub del_acl {
    my $self = shift;
    my %h;
    @h{@_} = (1) x @_;
    foreach my $node ($self->acl_nodes) {
	$node->drop if $h{acl_name($node, 1)}
    }
}

sub servers {
    my $self = shift;
    my @ret = map {
	my ($addr) = map { dequote($_->arg(0)) } $_->select(name => 'Address');
	my ($port) = map { $_->arg(0) } $_->select(name => 'Port');
	if ($addr && $port) {
	    "$addr:$port"
	} else {
	    ()
	}
    } $self->service->select(name => 'Backend');
    if (@_) {
	my $n = shift;
	return undef unless $n < @ret;
	return $ret[$n];
    }
    @ret;
}

sub name {
    my $self = shift;
    return dequote($self->service->arg(0))
}

sub drop {
    my $self = shift;
    $self->service->drop;
    $self->{_domains} = [];
    $self->mark_dirty;
}

1;

#
# FIXME
#
sub dequote {
    my ($text) = @_;
    my $q = ($text =~ s{^"(.*)"$}{$1});
    if ($q) {
	$text =~ s{\\(.)}{$1}g;
    }
    return $text;
}

1;
