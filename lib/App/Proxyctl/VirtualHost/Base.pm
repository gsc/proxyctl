package App::Proxyctl::VirtualHost::Base;
use strict;
use warnings;
use Carp;

sub new {
    my ($class, $file) = @_;
    bless { _backups => [], _file => $file }, $class;
}
    
sub is_dirty {
    my $self = shift;
    return $self->{_dirty}
}

sub mark_dirty {
    my $self = shift;
    $self->{_dirty} = 1;
}

sub clear_dirty {
    my $self = shift;
    $self->{_dirty} = 0;
}

sub normalize_hostnames {
    my $self = shift;
    map { lc } @_
}

sub file {
    my $self = shift;
    return $self->{_file};
}

sub writable {
    my $self = shift;
    -w $self->file;
}

sub _domainref {
    my $self = shift;
    unless ($self->{_domains}) {
	croak "can't use this method on invalid backend" unless $self->file;
	if (-e $self->file) {
	    open(my $fd, '<', $self->file)
		or croak "can't open ".$self->file.": $!";
	    chomp(my @domains = <$fd>);
	    close $fd;
	    $self->{_domains} = \@domains;
	} else {
	    $self->{_domains} = [];
	}
    }
    return $self->{_domains};
}

sub domains {
    my $self = shift;
    my $dom = $self->_domainref;
    if (defined(my $n = shift)) {
	return undef unless $n < @$dom;
	return $dom->[$n]
    }
    return @$dom;
}

sub has_domain {
    my ($self, $name) = @_;
    $name = lc $name;
    $name =~ s/\.$//;
    foreach my $dom ($self->domains) {
	return 1 if ($name eq $dom);
    }
    return 0
}

sub add_domain {
    my $self = shift;
    push @{$self->_domainref()}, $self->normalize_hostnames(@_);
    $self->mark_dirty;
}
    
sub del_domain {
    my $self = shift;
    my @hosts = $self->normalize_hostnames(@_);
    my %dl;
    @dl{@hosts} = (1) x @hosts;
    $self->{_domains} = [ 
	grep { 
	    if ($dl{$_}) {
		$self->mark_dirty;
		0;
	    } else {
		1
	    }
	} @{$self->_domainref()}
    ];
}

sub backup {
    my $self = shift;
    my $bkname = $self->file . '.~' . (@{$self->{_backups}} + 1) . '~';
    if (-f $self->file) {
	unless (rename $self->file, $bkname) {
	    # FIXME: rollback 
	    croak "failed to rename ".$self->file." to $bkname: $!";
	}
	push @{$self->{_backups}}, $bkname;
    } else {
	push @{$self->{_backups}}, undef;
    }
}
    
sub rollback {
    my $self = shift;
    my $name = pop @{$self->{_backups}};
    return unless $name;
    if (-f $self->file) {
	if (unlink($self->file)) {
	    croak "failed to unlink ".$self->file.": $; backup copy preserved in $name";
	}
    }
    unless (rename $name, $self->file) {
	croak "failed to restore ".$self->file." from $name: $!";
    }
    return $name
}
	
sub save {
    my $self = shift;
    if ($self->is_dirty) {
	$self->backup;
	if ($self->domains == 0) {
	    $self->clear_dirty;
	    return;
	}
		    
	my $u = umask(002);
	open(my $fd, '>', $self->file)
	    or croak "can't open ".$self->file." for writing: $!";
	umask($u);
	foreach my $dom ($self->domains) {
	    print $fd $dom,"\n";
	}
	close $fd;

	# Try to set owner to root
        chown 0, -1, $self->file;

	$self->clear_dirty
    }
}

sub DESTROY {
    my $self = shift;
    while (my $bk = pop @{$self->{_backups}}) {
	unlink $bk;
    }
}

1;
