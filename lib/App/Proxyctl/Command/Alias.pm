package App::Proxyctl::Command::Alias;
use strict;
use warnings;
use parent 'App::Proxyctl::Command';

# proxyctl alias --list|--add|--remove BE X Y Z
sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_,
	optmap => {
	    'list|l' => sub { shift->setcommand(\&list) },
	    'add|a' => sub { shift->setcommand(\&add) },
	    'remove|delete|r' => sub { shift->setcommand(\&delete) },
	    'host|h' => 'host'
	});
    return $self;
}

sub setcommand {
    my ($self, $com) = @_;
    if ($self->{subcommand}) {
	$self->usage_error("only one of --list, --add, --delete can be used");
    }
    $self->{subcommand} = $com;
}

sub run {
    my $self = shift;
    $self->SUPER::run();
    my $subcommand = $self->{subcommand} // \&list;
    $self->$subcommand();
}

sub list {
    my $self = shift;
    my $vhost = $self->find_vhost;
    $self->usage_error("too many arguments")
	if @ARGV;
    foreach my $dom ($vhost->domains) {
	print "$dom\n";
    }
}

sub add {
    my $self = shift;
    my $vhost = $self->find_vhost;
    $self->usage_error("one or more aliases expected")
	unless @ARGV;
    $self->ensure_write_permissions($vhost);
    my @hosts = grep {
	if ($vhost->has_domain($_)) {
	    $self->error("$_: alias already exists");
	    0
	} else {
	    1
        }
    } @ARGV;
    $self->usage_error("nothing to add") unless @hosts;
    $self->check_domains(@hosts);
    $vhost->add_domain(@hosts);
    $vhost->save;
    $self->register_cname(@hosts);
    $self->rc('modify', $vhost);
}

sub delete {
    my $self = shift;
    my $vhost = $self->find_vhost;
    $self->usage_error("one or more aliases expected")
	unless @ARGV;
    $self->ensure_write_permissions($vhost);
    $self->usage_error("virtual host has no aliases; use proxyctl drop to remove it")
	if $vhost->domains == 1;
    my @hosts = grep {
	if ($vhost->has_domain($_)) {
	    1
	} else {
	    $self->error("$_: no such alias");
	    0
        }
    } @ARGV;
    $self->usage_error("nothing to delete") unless @hosts;
    $vhost->del_domain(@hosts);
    $vhost->save;
    $self->deregister_cname(@hosts);
    $self->rc('modify', $vhost);
}

1;
__END__

=head1 NAME

proxyctl-alias - manipulate virtual host hostnames
    
=head1 SYNOPSIS

B<proxyctl alias>    
[B<-Dlah>]
[B<--list>]
[B<--add>]
[B<--remove>]
[B<--delete>]
[B<--host>]
I<VHOST>
[I<HOSTNAME>...]
    
=head1 DESCRIPTION

The B<alias> command lists, adds, or deletes hostnames from a virtual host
configuration. The virtual host to operate upon is indicated by the I<VHOST>
argument. Normally it is treated as the name of the virtual host. In presence
of B<-h> (B<--host>) option however, the I<VHOST> argument is treated as a
hostname. In this case, the utility selects the virtual host that has I<VHOST>
among its configured hostnames.
    
=head1 OPTIONS

=over 4

=item B<-l>, B<--list>

List hostnames for I<VHOST>. This is the default.

=item B<-a>, B<--add>

Add hostnames to the virtual host configuration.

=item B<-r>, B<--remove>, B<--delete>

Delete hostnames from the virtual host configuration.

=item B<-h>, B<--host>

Treat I<VHOST> as a hostname. Find such a virtual host that has I<VHOST>
among its configured hostnames and operate on it.

=back    
    
=head1 SEE ALSO

B<proxyctl> (1).

=cut
