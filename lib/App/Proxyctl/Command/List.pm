package App::Proxyctl::Command::List;
use strict;
use warnings;
use parent 'App::Proxyctl::Command';

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_,
	optdef => {
	    long => 0
	},
	optmap => {
	    'long|l' => 'long'
	});
    return $self;
}

sub run {
    my $self = shift;
    $self->SUPER::run();
    foreach my $vhost ($self->virtual_hosts) {
	next if @ARGV && ! grep { $vhost->name eq $_ } @ARGV;
	printf("%-16s %-24s %s", $vhost->name, $vhost->servers(0),
	       $vhost->domains(0));
	my $n = $vhost->domains - 1;
	if ($n > 0) {
	    printf(" (%d aliases)", $n);
	}
	print "\n";
	if ($self->option('long')) {
	    print "  backends: ".join(',', $vhost->servers)."\n";
	    print "  domains: ".join(',', $vhost->domains)."\n";
	    if (my @acl = $vhost->acl) {
		print "  ACL: ".join(' ',@acl)."\n";
	    }
#	    printf("%-16s %s:\n", $vhost->node->locus, $vhost->name);
	}
    }
}
1;
__END__
=head1 NAME

proxyctl-list - list virtual hosts

=head1 SYNOPSIS

B<proxyctl list>
[B<-l>]
[B<--long>]    
[I<NAME>...]

=head1 DESCRIPTION

Lists configured virtual hosts. Optional arguments are names of the virtual
hosts to list.

Default output format lists each host on a separate line as a series of
the following fields, delimited with whitespace:

=over 4

=item Virtual host name

=item Backend (IP address and port)

=item First hostname of the virtual host

=back

If virtual host is configured for several hostnames, the number of aliases
is listed at the end of line.    
    
=head1 OPTIONS

=over 4

=item B<-l>, B<--long>

Print detailed virtual host information after the initial description.
The additional information is formatted as C<keyword: value> pairs, each
on a separate line and indented two columns.  The information lists
configured backends, full list of domain names and ACLs (if any) protecting
the frontend.

=back

=head1 SEE ALSO

B<proxyctl> (1),
B<proxyctl alias> (1).    

    
