package App::Proxyctl::Command::Add;
use strict;
use warnings;
use parent 'App::Proxyctl::Command';

sub new {
    my $class = shift;
    return $class->SUPER::new(@_,
			      optmap => {
				  'port|p=n' => 'port',
				  'acl=s' => 'acl'    
			      });
}

sub run {
    my $self = shift;
    $self->SUPER::run();

    my $name = shift @ARGV
	or $self->usage_error("virtual host name must be given");
    $self->usage_error("at least one host name must be given")
	unless @ARGV;
    my $acl;
    if ($acl = $self->option('acl')) {
	$self->check_acl($acl);
    }

    $self->check_vhost_name($name);
    $self->check_domains(@ARGV);

    $self->ensure_write_permissions;

    my $port = $self->get_port($self->option('port'));
    my $vhost = $self->vhost->create($self, $name, $port,
		 domains => \@ARGV,
		 condition => $acl);
    $self->save($vhost);
    $self->register_cname(@ARGV);
    $self->rc('add', $vhost);
    print $vhost->servers(0), "\n";
}

sub check_vhost_name {
    my ($self, $name) = @_;
    foreach my $vhost ($self->virtual_hosts) {
	if ($vhost->name eq $name) {
	    $self->abend(1, "virtual host "
			 . $vhost->name
			 . " already exists ("
			 . $vhost->locus
			 . ")");
	}
    }
    if (my $err = $self->vhost->check_name($name, $self->config)) {
	$self->abend(1, $err)
    }
}

1;
__END__

=head1 NAME

proxyctl-add - add a virtual host
    
=head1 SYNOPSIS

B<proxyctl add>
[B<-p> I<PORT>]
[B<--acl> I<ACL>]
[B<--port=>I<PORT>]    
I<VHOST>
I<HOSTNAME>...    

=head1 DESCRIPTION

Configures new virtual host with name I<VHOST> and one or more I<HOSTNAME>s.
The I<VHOST> argument is a unique name which will be used with other
B<proxyctl> commands to identify the virtual host to operate upon.   

Unless I<PORT> is supplied, the program allocates next available port
number from the range [B<core.first-port>, B<core.last-port>]. It will
abend if all ports in the range are allocated.

If one or more B<--acl> options are given, the created frontend will be
protected by the given I<ACL>s.  I<ACL>s must refer to access control lists
defined in the F<haproxy.cfg> file, either in its global section or in
the I<https> frontend.

On success, the program prints the IP address and port assigned for the
backend of the newly created virtual host.    

=head1 OPTIONS

=over 4

=item B<--acl=>[B<!>]I<ACL>

Protect the frontend by the given I<ACL>.  Multiple B<--acl> options are
allowed.  I<ACL> must be defined in the F<haproxy.cfg> configuration file.
If preceded with a B<!>, the frontend will be activated if the connection
does not satisfy the I<ACL>.

=item B<-p>, B<--port=>I<N>

=back

=head1 SEE ALSO

B<proxyctl> (1),
B<proxyctl-listacl> (1).
