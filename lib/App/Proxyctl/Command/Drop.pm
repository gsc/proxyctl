package App::Proxyctl::Command::Drop;
use strict;
use warnings;
use parent 'App::Proxyctl::Command';

sub run {
    my $self = shift;
    $self->SUPER::run();
    my $vhost = $self->find_vhost;
    $self->ensure_write_permissions($vhost);
    my @domains = $vhost->domains;
    $vhost->drop();
    $self->save($vhost);
    $self->deregister_cname(@domains);
    $self->rc('remove', $vhost);
}

1;
__END__

=head1 NAME

proxyctl-drop - removes virtual host from the proxy configuration
    
=head1 SYNOPSIS

B<proxyctl drop> I<VHOST>
    
=head1 DESCRIPTION

Deregisters hostnames assigned to the virtual host I<VHOST> and removes it
from the proxy configuration.    

=head1 SEE ALSO

B<proxyctl> (1).

=cut
