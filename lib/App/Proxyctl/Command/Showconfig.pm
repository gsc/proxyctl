package App::Proxyctl::Command::Showconfig;
use strict;
use warnings;
use parent 'App::Proxyctl::Command';

sub new {
    my $class = shift;
    $class->SUPER::new(@_,
	optmap => {
	    'indent|i=n' => sub {
		my $self = shift;
		$self->{_saveparam}{indent} = $_[1];
	    },
	    'reindent-comments|c' => sub {
		my $self = shift;
		$self->{_saveparam}{reindent_comment} = $_[1];
	    },
	    'tabstops|t=s' => sub {
		my $self = shift;
		$self->{_saveparam}{tabstop} = [ split /,/, $_[1] ]
	    }
	});
}

sub run {
    my $self = shift;
    $self->SUPER::run();
    $self->config->write(\*STDOUT, %{$self->{_saveparam} // {}});
}
1;
__END__

=head1 NAME

proxyctl-showconfig - shows the proxy configuration    
    
=head1 SYNOPSIS

B<proxyctl showconfig>
[B<-c>]
[B<-i> I<N>]
[B<-t> I<TABSTOPS>]
[B<--indent=>I<N>]
[B<--reindent-comments>]
[B<--tabstops=>I<TABSTOPS>]
    
=head1 DESCRIPTION

Displays on the standard output the HAProxy server configuration, optionally
reindenting it.    
    
=head1 OPTIONS

=over 4

=item B<-i>, B<--indent=>I<N>

Sets the base indenting offset.

=item B<-c>, B<--reindent-comments>

Reindent comment lines as well as statements.

=item B<t>, B<--tabstops=>I<TABSTOPS>
    
Sets the list of tabstops. The argument is a comma-separated list of
tabstop columns.    

=back

=head1 SEE ALSO

B<proxyctl>

=cut

 
