package App::Proxyctl::Command::Cnames;
use strict;
use warnings;
use parent 'App::Proxyctl::Command';

my %opcode = (
    up => \&App::Proxyctl::Command::register_cname,
    down => \&App::Proxyctl::Command::deregister_cname
);

sub run {
    my $self = shift;
    $self->SUPER::run();
    my $opname = shift @ARGV
	or $self->usage_error("operation name (up or down) expected");
    $self->usage_error("too many arguments")
	if @ARGV;
    if (my $op = $opcode{$opname}) {
	foreach my $vhost ($self->virtual_hosts) {
	    foreach my $host ($vhost->domains) {
		$self->$op($host);
	    }
	}
	$self->rc;
    } else {
	$self->usage_error("unknown operation")
    }
}

1;

__END__

=head1 NAME

proxyctl-cnames - manipulate DNS CNAME records
    
=head1 SYNOPSIS

B<proxyctl cnames up>

B<proxyctl cnames down>    
    
=head1 DESCRIPTION

The B<cnames> tool creates or deletes the DNS CNAME records for the configured
virtual hosts. The command takes a single mandatory argument, identifying the
operation to perform.    

The B<up> operation instructs B<cnames> to register cnames and is normally used
during the system start up.

The B<down> operation deletes cnames corresponding to the configured virtual
host names and is normally used as a part of system shut down sequence.    

=head1 SEE ALSO

B<proxyctl> (1).

=cut
