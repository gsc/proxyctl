package App::Proxyctl::Command::Vars;
use strict;
use warnings;
use parent 'App::Proxyctl::Command';

sub run {
    my $self = shift;
    print $self->ini->canonical,"\n";
}

1;
__END__

=head1 NAME

proxyctl-vars - shows configuration variables

=head1 SYNOPSIS

B<proxyctl vars>

=head1 DESCRIPTION

Prints on standard output B<proxyctl> configuration variables and their
values in unambiguous format.

=head1 SEE ALSO

B<proxyctl>(1).

=cut

   
