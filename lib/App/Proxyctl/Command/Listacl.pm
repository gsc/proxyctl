package App::Proxyctl::Command::Listacl;
use strict;
use warnings;
use parent 'App::Proxyctl::Command';
use Data::Dumper;
use Text::Locus;

sub new {
    my $class = shift;
    return $class->SUPER::new(@_,
			      optmap => {
				  'long|l' => 'long'
			      });
}

sub run {
    my $self = shift;
    $self->SUPER::run();

    my $aclist = $self->vhost->acl_list($self->config);
    unless (@ARGV) {
	push @ARGV, sort keys %$aclist
    }
	
    foreach my $a (@ARGV) {
	if (my $acl = $aclist->{$a}) {
	    print $acl->locus.": $a\n";
	    if ($self->option('long')) {
		$acl->write;
		print "\n";
	    }
	} else {
	    $self->error("ACL $a does not exist!");
	}
    }
}

1;

__END__
=head1 NAME

proxyctl-listacl - list configured ACLs

=head1 SYNOPSIS

B<proxyctl listacl>
[B<-l>]
[B<--long>]    
[I<NAME>...]

=head1 DESCRIPTION

Lists ACLs defined in the F<haproxy.cfg> file.  By default, for each ACL
shows its location in the configuration file and ACL name.

=head1 OPTIONS

=over 4

=item B<-l>, B<--long>

Long output format.  Includes ACL definitions.

=back

=head1 SEE ALSO

B<proxyctl> (1),
B<proxyctl-acl> (1).
