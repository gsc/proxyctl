package App::Proxyctl::Command::Acl;
use strict;
use warnings;
use parent 'App::Proxyctl::Command';

sub new {
    my $class = shift;
    return $class->SUPER::new(@_,
			      optmap => {
				  'list|l' => sub { shift->setcommand(\&list) },
				   'add|a' => sub { shift->setcommand(\&add) },
				   'remove|delete|r' => sub { shift->setcommand(\&delete) },
			      });
}

sub setcommand {
    my ($self, $com) = @_;
    if ($self->{subcommand}) {
	$self->usage_error("only one of --list, --add, --delete can be used");
    }
    $self->{subcommand} = $com;
}

sub run {
    my $self = shift;
    $self->SUPER::run();
    my $subcommand = $self->{subcommand} // \&list;
    $self->$subcommand();
}

sub list {
    my $self = shift;
    my $vhost = $self->find_vhost;
    $self->usage_error("too many arguments") if @ARGV;
    if (my @acl = $vhost->acl) {
	print join(' ',@acl)."\n";
    }
}

sub add {
    my $self = shift;
    my $vhost = $self->find_vhost;
    $self->usage_error("one or more ACL conditions expected") unless @ARGV;
    if (my @matches = $self->vhost_acl_intersection($vhost, @ARGV)) {
	$self->error("These ACLs are already configured: " .
		     join(', ', @matches));
	exit(1);
    }
    $self->check_acl(@ARGV);
    $vhost->set_acl(@ARGV);
    $self->save;
    $self->rc('modify', $vhost);
}

sub delete {
    my $self = shift;
    my $vhost = $self->find_vhost;
    my @acl = $vhost->acl;
    $self->check_acl(@ARGV);
    foreach my $a (@ARGV) {
	$self->usage_error("not protected by ACL $a")
	    unless grep { $a eq $_ } @acl;
    }
    $vhost->del_acl(@ARGV);
    $self->save;
    $self->rc('modify', $vhost);
}

1;
__END__
=head1 NAME

proxyctl-acl - configure ACLs for the virtual host

=head1 SYNOPSIS

B<proxyctl acl>
[B<-alr>]
[B<--list>]
[B<--add>]
[B<--delete>]
[B<--remove>]
I<VHOST>
[[B[!]I<ACL>...]

=head1 DESCRIPTION

Lists, adds or removes ACLs for the given virtual host.  The default action
is list.

=head1 OPTIONS

=over 4

=item B<-a>, B<--add>

Adds ACLs to the frontend definition of the virtual host I<VHOST>.  At
least one I<ACL> must be specified.  Use B<proxyctl listacl> to list
available I<ACL> names.

=item B<-l>, B<--list>

List ACLs for the given I<VHOST>.  Takes exactly one argument.

=item B<-r>, B<--remove>, B<--delete>

Removes ACLs from the frontend definition.  Arguments must be given exactly
as they appear in the frontend definition.

=back

ACLs are identified by their name, optionally prefixed with B<!>.  The latter
form negates it.  For example,

   proxyctl acl --add myhost '!remote'

will configure the virtual host B<myhost> to be accessible from IP addresses
not in the ACL B<remote>.

=head1 SEE ALSO

B<proxyctl> (1),
B<proxyctl-listacl> (1).
