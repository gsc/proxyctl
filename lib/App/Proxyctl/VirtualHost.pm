package App::Proxyctl::VirtualHost;
use strict;
use warnings;
use Carp;
    
sub new {
    my ($class, $impl) = @_;
    return bless { impl => $impl }, $class
}

sub load {
    my $self = shift;
    my $meth = shift;
    my $modname = __PACKAGE__ . '::' . $self->{impl};
    my $modpath = $modname;
    $modpath =~ s{::}{/}g;
    $modpath .= '.pm';
    my $mod = eval {
	require $modpath;
	$modname->${ \$meth }(@_);
    };
    if ($@) {
	if ($@ =~ /Can't locate $modpath/) {
	    croak "unsupported proxy implementation: $self->{impl}"
	} else {
	    croak $@
	}
    }
    return $mod
}

our $AUTOLOAD;
sub AUTOLOAD {
    my $self = shift;
    $AUTOLOAD =~ s/(?:.*::)?(.+)//;
    return $self->load($1, @_);
}

sub DESTROY { }

1;
