package App::Proxyctl::Command;
use strict;
use warnings;
use Getopt::Long qw(:config gnu_getopt no_ignore_case require_order);
use Pod::Man;
use Pod::Usage;
use Pod::Find qw(pod_where);
use File::Basename;
use File::Spec;
use App::Proxyctl::NSUpdate;
use App::Proxyctl::VirtualHost;
use Config::Proxy;
use Carp;
use Taint::Util;
use parent 'Exporter';

use constant {
    EX_OK => 0,
    EX_FAIL => 1,
    EX_USAGE => 2
};

our @EXPORT_OK = qw(EX_OK EX_FAIL EX_USAGE);
our %EXPORT_TAGS = ( 'exit_codes' => [
			 qw(EX_OK EX_FAIL EX_USAGE)
		     ] );

sub new {
    my $class = shift;
    my $ini = shift;
    my $self = bless {
	_debug => 0,
	_dry_run => 0,
	_progname => basename($0),
	_ini => $ini,
	_vhost => App::Proxyctl::VirtualHost->new($ini->core->proxy)
    }, $class;
    my %opts;
    local %_ = @_;
    $self->{_options} = delete $_{optdef} // {};
    if (defined(my $optmap = delete $_{optmap})) {
	croak "optmap must be a hashref" unless ref($optmap) eq 'HASH';
	foreach my $k (keys %{$optmap}) {
	    if (ref($optmap->{$k}) eq 'CODE') {
		$opts{$k} = sub { &{$optmap->{$k}}($self, @_ ) }
	    } elsif (ref($optmap->{$k})) {
		$opts{$k} = $optmap->{$k};
	    } else {
		$self->{_options}{$optmap->{$k}} = undef
		    unless exists($self->{_options}{$optmap->{$k}});
		$opts{$k} = \$self->{_options}{$optmap->{$k}};
	    }
	}
    }

    croak "unrecognized parameters" if keys(%_);

    $opts{'shorthelp|?'} = sub {
	$) = $(;
	pod2usage(-message => $self->pod_usage_msg,
		  -input => pod_where({-inc => 1}, ref($self)),
		  -exitstatus => EX_OK)
    };
    $opts{help} = sub {
	# Reset EGID to real GID, to avoid insecure dependency error.
	$) = $(;
	pod2usage(-exitstatus => EX_OK,
		  -verbose => 2,
		  -input => pod_where({-inc => 1}, ref($self)))
    };
    $opts{usage} = sub {
	$) = $(;
	pod2usage(-exitstatus => EX_OK,
		  -verbose => 0,
		  -input => pod_where({-inc => 1}, ref($self)))
    };
    $opts{'debug|d'} = sub { $self->{_debug}++ };
    $opts{'dry-run|n'} = sub { $self->{_debug}++; $self->{_dry_run} = 1 };
    $opts{'program-name=s'} = sub { $self->{_progname} = $_[1] };
    $opts{'directory|C=s'} = \$self->{_confdir};
    $self->{_optmap} = \%opts;

    $self->{_lint_enable} = 1;
    if (${^TAINT}) {
	require Module::Metadata;
	if ($Module::Metadata::VERSION < 1.000017) {
	    # See https://rt.cpan.org/Public/Bug/Display.html?id=88576
	    warn "Module::Metadata is too old ($Module::Metadata::VERSION): configuration syntax check is disabled.\n";
	    warn "Please report to the system administrator.\n";
	    $self->{_lint_enable} = 0;
	}
    }

    return $self;
}

sub run {
    my $self = shift;
    GetOptions(%{$self->{_optmap}}) or exit(EX_USAGE);
}

sub dry_run { shift->{_dry_run} }

sub progname {
    my $self = shift;
    if (defined(my $v = shift)) {
	croak "too many arguments" if @_;
	$self->{_progname} = $v;
    }
    $self->{_progname};
}

sub vhost { shift->{_vhost} }

sub option {
    my $self = shift;
    my $name = shift;
    if (defined(my $v = shift)) {
	croak "too many arguments" if @_;
	$self->{_options}{$name} = $v;
    }
    return $self->{_options}{$name};
}

sub debug {
    my ($self, $l, @msg) = @_;
    if ($self->{_debug} >= $l) {
	print STDERR "$self->{_progname}: " if $self->{_progname};
	print STDERR "DEBUG: ";
	print STDERR "@msg\n";
    }
}

sub error {
    my ($self, @msg) = @_;
    print STDERR "$self->{_progname}: " if $self->{_progname};
    print STDERR "@msg\n";
}

sub abend {
    my ($self, $code, @msg) = @_;
    $self->error(@msg);
    exit $code;
}

sub usage_error {
    my $self = shift;
    $self->abend(EX_USAGE, @_);
}

sub pod_usage_msg {
    my ($self) = @_;
    my %args;

    my $msg = "";

    open my $fd, '>', \$msg;

    if (defined($self)) {
	if (my $r = ref($self)) {
	    $self = $r;
	}
	$args{-input} = pod_where({-inc => 1}, $self);
    }

    pod2usage(-verbose => 99,
	      -sections => 'NAME',
	      -output => $fd,
	      -exitval => 'NOEXIT',
	      %args);

    my @a = split /\n/, $msg;
    if ($#a < 1) {
	croak "missing or malformed NAME section in " . ($args{-input} // $0);
    }
    $msg = $a[1];
    $msg =~ s/^\s+//;
    $msg =~ s/ - /: /;
    return $msg;
}

sub ini { shift->{_ini} }

sub proxy_conf_get {
    my ($self, $key) = @_;
    return $self->ini->get($self->ini->core->proxy, $key)
}

sub confdir { shift->proxy_conf_get('confdir') }

sub conffile { shift->proxy_conf_get('conffile') }

sub hostdir { shift->proxy_conf_get('hostdir') }

sub linter { shift->proxy_conf_get('check') }

# Return nsupdate configuration for the given hostname
sub nsupdate_config {
    my $self = shift;
    my $name = shift;
    if ($self->ini->nsupdate) {
	my ($domain) = sort {
	                  length($b) <=> length($a)
	               }
	               grep {
			   my $rx = '^[a-zA-Z0-9-.]+\.' . qr($_) . '$';
			   $name =~ m{$rx}
		       } ($self->ini->nsupdate->keys);
	if ($domain) {
	    my %cfg = %{$self->ini->nsupdate->subtree($domain)->as_hash};
	    if ($self->ini->core->hostname) {
		$cfg{target} = $self->ini->core->hostname->value;
	    }
	    if (my $zone = delete($cfg{zone})) {
		$domain = $zone;
	    }
	    return ($domain, \%cfg);
	}
    }
}

sub config {
    my $self = shift;
    unless ($self->{_config}) {
	my $cfg = new Config::Proxy($self->ini->core->proxy, $self->conffile);
	$cfg->lint($self->{_lint_enable});
	if (my $cmd = $self->linter) {
	    $cfg->lint(command => $cmd);
	}
	$cfg->parse();
	$self->{_config} = $cfg;
    }
    return $self->{_config};
}

sub frontend {
    my $self = shift;
    $self->vhost->frontend($self->config)
}

sub virtual_hosts {
    my $self = shift;
    unless ($self->{_virtual_hosts}) {
	$self->{_virtual_hosts} = $self->vhost->virtual_hosts($self);
    }
    if (defined(my $n = shift)) {
	return $self->{_virtual_hosts}[$n];
    }
    return @{$self->{_virtual_hosts}};
}

sub check_domains {
    my $self = shift;
    my %dh;

    exit(1) unless $self->valid_hostnames(@_);
    
    foreach my $vh ($self->virtual_hosts) {
	foreach my $dom ($vh->domains) {
	    $dh{$dom} = $vh;
	}
    }
    my $err = 0;
    foreach my $dom (@_) {
	if ($dh{$dom}) {
	    $self->error("$dom: already defined in virtual host "
			 . $dh{$dom}->name
			 . ' ('
			 . $dh{$dom}->node->locus
			 . " and "
			 . $dh{$dom}->backend->locus
			 . ")");
	    $err = 1;
	}
    }
    exit(1) if $err;
}

sub valid_hostnames {
    my $self = shift;
    my $ok = 1;
    foreach my $host (@_) {
	if ($host !~ m{^[a-zA-Z0-9-.]+$}) {
	    $self->error("$host: invalid hostname");
	    $ok = 0;
	}
    }
    return $ok;
}

sub port_list {
    my $self = shift;
    return map {
	my $addr = $_->servers(0);
	if ($addr =~ s/127\.0\.0\.1://) {
	    $addr
	} else {
	    ()
	}
    } $self->virtual_hosts;
}

sub port_alloc {
    my $self = shift;
    my @allocated = sort { $b <=> $a } $self->port_list;
    my $port = shift @allocated;
    return $self->ini->core->first__port->value unless $port;
    if ($port == $self->ini->core->last__port->value) {
	while (@allocated) {
	    if ($port - $allocated[0] > 1) {
		return $allocated[0] + 1;
	    }
	    $port = shift @allocated;
	}
	$self->abend(1, "no free ports left");
    }
    return $port + 1;    
}

sub port_is_allocated {
    my ($self, $port) = @_;
    grep { $_ == $port } $self->port_list;
}

sub get_port {
    my ($self, $port) = @_;
    if ($port) {
	if ($port < $self->ini->core->first__port->value
	    || $port > $self->ini->core->last__port->value) {
	    $self->abend(1, "port $port is out of allowed range");
	} elsif ($self->port_is_allocated($port)) {
	    $self->abend(1, "port $port is already allocated");
	}
    } else {
	$port = $self->port_alloc();
    }
    return $port;
}

sub find_vhost {
    my $self = shift;
    my $name = shift @ARGV;
    $self->usage_error("virtual host or domain name must be given")
	unless $name;
    foreach my $vhost ($self->virtual_hosts) {
	if ($self->option('host')) {
	    if ($vhost->has_domain($name)) {
		return $vhost;
	    }
	} elsif ($vhost->name eq $name) {
	    return $vhost;
	}
    }
    $self->abend(1, "no matching virtual host found");
}

sub nsupdate {
    my ($self, $host) = @_;
    my ($zone,$cfg) = $self->nsupdate_config($host);
    if ($zone) {
	unless (defined($self->{_nsupdate}{$zone})) {
	    my $upd;
	    eval {
		$upd = new App::Proxyctl::NSUpdate($zone, $self, %$cfg);
	    };
	    if ($@) {
		delete $self->{_nsupdate_config};
		$self->error($@);
	    }
	    $self->{_nsupdate}{$zone} = $upd;
	}
	return $self->{_nsupdate}{$zone};
    }
}

sub register_cname {
    my $self = shift;
    foreach my $host (@_) {
	if (my $upd = $self->nsupdate($host)) {
	    $self->debug(1, "registering $host");
	    $upd->register($host)
	}
    }
}

sub deregister_cname { 
    my $self = shift;
    foreach my $host (@_) {
	if (my $upd = $self->nsupdate($host)) {
	    $upd->deregister($host)
	}
    }
}

sub runcom {
    my ($self, $cmd) = @_;
    $self->debug(1, "running $cmd");
    system($cmd);
    if ($? == -1) {
	$self->error("can't run $cmd: $!");
    } elsif ($? & 127) {
	$self->error("$cmd: died on signal "
		     . ($? & 127)
		     . (($? & 128) ? ' (core dumped)' : ''));
    } elsif (my $code = $? >> 8) {
	$self->error("$cmd: exit code $code");
    }
}

sub rc {
    my ($self, $action, $vhost) = @_;
    return unless $self->ini->core->rc;
    if ($action) {
	$ENV{VHOST_ACTION} = $action;
    } else {
 	delete $ENV{VHOST_ACTION};
    }
    if ($vhost) {
	$ENV{VHOST_NAME} = $vhost->name;
	$ENV{VHOST_FILE} = $vhost->file;
    } else {
	delete $ENV{VHOST_NAME};
	delete $ENV{VHOST_FILE};
    }
    foreach my $cmd (($self->ini->core->rc->value)) {
	$self->runcom($cmd);
    }
}

sub can_write {
    my ($self) = @_;
    return -w $self->confdir && -w $self->conffile && -w $self->hostdir
}

sub ensure_write_permissions {
    my ($self,$vhost) = @_;
    $self->abend(EX_FAIL, "can't modify configuration")
	unless $self->can_write;
    if ($vhost) {
	$self->abend(EX_FAIL, "virtual host configuration is not writable")
	    unless $vhost->writable();
    }
}

sub save {
    my $self = shift;
    foreach my $vhost (@_) {
	$vhost->save;
    }
    eval {
	$self->config->save(indent => 4);
    };
    if ($@) {
	while (my $vhost = pop @_) {
	    $vhost->rollback;
	}
	if ($@ =~ s/Syntax check failed://) {
	    $@ =~ s/at .+? line \d+\.$//;
	    $self->error("resulting configuration file fails syntax check");
	}

	foreach my $msg (split /\n+/, $@) {
	    next if $msg eq '';
	    last if $msg =~ /Error\(s\) found in configuration file/;
	    $self->error($msg);
	}
	$self->error("all changes have been reverted");
	$self->abend(EX_FAIL, "contact system administrator for assistance");
    }
}

sub check_acl {
    my ($self, $acl) = @_;
    my $acl_list = $self->vhost->acl_list($self->config);
    foreach my $a (split /\s+/, $acl) {
	if ($a =~ s/^!?([_a-zA-Z][_a-zA-Z0-9-]+)$/$1/) {
	    $self->usage_error("no such acl: $a")
		unless exists $acl_list->{$a};
	} else {
	    $self->usage_error("invalid acl name: $acl")
	}
    }
}

sub vhost_acl_intersection {
    my ($self, $vhost, @acls) = @_;
    my %h;
    my @s = map { s/^!//; $_ } @acls;
    @h{@s} = (1) x @s;
    my @ret;
    foreach my $acl (map { s/^!//; $_ } $vhost->acl) {
	push @ret, $acl if $h{$acl};
    }
    return @ret
}

1;
