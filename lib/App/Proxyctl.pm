package App::Proxyctl;
use strict;
use warnings;
use App::Proxyctl::Config;
use Carp;
use Getopt::Long qw(:config gnu_getopt no_ignore_case require_order);
use Pod::Man;
use Pod::Usage;
use Pod::Find qw(pod_where);
use App::Proxyctl::Command qw(:exit_codes);

our $VERSION = '2.00';

sub new {
    my $class = shift;

    # Make sure PATH is safe
    $ENV{PATH} = '/usr/local/bin:/usr/bin:/bin';

    GetOptions(
	'shorthelp|?' => sub {
	    $) = $(;
	    pod2usage(-input => pod_where({-inc => 1}, __PACKAGE__),
		      -verbose => 99,
		      -sections => [qw(NAME SYNOPSIS COMMANDS)],
		      -exitstatus => EX_OK)
	},
	'help' => sub {
	    # Reset EGID to real GID, to avoid insecure dependency error.
	    $) = $(;
	    pod2usage(-exitstatus => EX_OK,
		      -input => pod_where({-inc => 1}, __PACKAGE__),
		      -verbose => 2)
	},
	'usage' => sub {
	    $) = $(;
	    pod2usage(-exitstatus => EX_OK,
		      -input => pod_where({-inc => 1}, __PACKAGE__),
		      -verbose => 0)
	}) or exit(EX_USAGE);
    my $config = new App::Proxyctl::Config(filename => '/etc/proxyctl.conf');
    return $class->command($config)
}

sub command {
    my $class = shift;
    my $config = shift;

    my $com = shift @ARGV;
    die "no command name\n" unless $com;
    my $modname = __PACKAGE__ . '::Command::' . ucfirst($com);
    my $modpath = $modname;
    $modpath =~ s{::}{/}g;
    $modpath .= '.pm';
    my $cmd;
    eval {
	require $modpath;
	$cmd = $modname->new($config);
    };
    if ($@) {
	if ($@ =~ /Can't locate $modpath/) {
	    die "unknown command: $com\n"
	}
	die $@;
    }
    return $cmd;
}

1;
__END__

=head1 NAME

proxyctl - proxy control interface

=head1 SYNOPSIS

B<proxyctl> I<COMMAND> I<OPTIONS> I<ARG>...

B<proxyctl> B<-?> B<--help> B<--usage>

=head1 DESCRIPTION

User control interface for B<HAProxy> and <Pound> proxy servers.

=head1 COMMANDS

The following commands are implemented:

=head2 acl

    proxyctl acl [OPTIONS] VHOST [ACL...]

Manages ACLs for a virtual host.  See B<proxyctl-acl>(1), for details.

=head2 add

    proxyctl add VHOST HOSTNAME...

This command creates new virtual host. The I<VHOST> argument supplies a
unique name which serves to identify this virtual host. One or more
I<HOSTNAME> arguments supply the symbolic hostnames for that host.

On success, the program prints the IP address and port assigned for the
backend of the newly created virtual host.

See B<proxyctl-add>(1), for details.

=head2 alias

    proxyctl alias [--list] VHOST
    proxyctl alias --add VHOST HOSTNAME...
    proxyctl alias --remove VHOST HOSTNAME...

Lists, adds, or deletes hostnames from a virtual host configuration.

See B<proxyctl-alias>(1), for details.

=head2 cnames

    proxyctl cnames up
    proxyctl cnames down

Creates or removes the DNS CNAME records for all configured virtual hosts.
Useful in system startup and shutdown scripts.

See B<proxyctl-cnames>(1), for details.

=head2 drop

    proxyctl drop VHOST

Removes virtual host from the proxy configuration, frees its backend and
removes any associated CNAME records from the DNS.

See B<proxyctl-drop>(1), for details.

=head2 list

    proxyctl list [VHOST...]

Lists configured virtual hosts. To list only selected virtual hosts, supply
their names as arguments.

See B<proxyctl-list>(1), for details.

=head2 showconfig

    proxyctl showconfig

Displays on the standard output the HAProxy server configuration. Options
can be used to reindent the configuration.

See B<proxyctl-showconfig>(1), for details.

=head2 vars

    proxyctl vars

Displays B<proxyctl> configuration variables and their
values in unambiguous format.

See B<proxyctl-vars>(1), for details.

=head1 CONFIGURATION

Configuration settings are kept in file F</etc/proxyctl.conf>. The format
is similar to the B<git>(1) configuration file.

=head2 [core]

This section defines global variables:

=over 4

=item B<hostname>

Sets the DNS name of the host. This name will be used as a target for
B<CNAME> records.

Normally the hostname is determined automatically. Use this variable if
the canonical name of the server differs from the one returned by the
B<hostname>(1) command.

=item B<first-port>

Number of the first allocatable port number for backend. Defaults to 8080.

=item B<last-port>

The highest allocatable port number. Defaults to 65353.

=item B<proxy>

Proxy server to use.  Allowed values are: B<haproxy> and B<pound>.  Default
is B<haproxy>.  Depending on this value, either B<[haproxy]> or
B<[pound]> section will be used (see below).

=item B<rc>

Defines external command that will be run after modifying the configuration.
Multiple B<rc> keywords will be processed in the order of their appearance
in the configuration file.

=back

=head2 [haproxy]

This section defines paths for B<haproxy> configuration files.

=over 4

=item B<confdir>

Name of the B<haproxy> configuration directory. Defaults to F</etc/haproxy>.

=item B<conffile>

Name of the B<haproxy> configuration file. Defaults to
F<B<$confdir>/haproxy.cfg>.

=item B<hostdir>

Name of the directory where hostname list files are kept. Defaults to
F<B<$confdir>/hosts>.

=item B<check>

Command to check syntax of the HAProxy configuration file. It will be
called with the filename as argument. Default is B<haproxy -c -f>.

=back

=head2 [pound]

This section defines paths for B<pound> configuration files.

=over 4

=item B<confdir>

Name of the B<pound> configuration directory. Defaults to F</etc/pound>.

=item B<conffile>

Name of the B<pound> configuration file. Defaults to
F<B<$confdir>/pound.cfg>.

=item B<hostdir>

Name of the directory where hostname list files are kept. Defaults to
F<B<$confdir>/hosts>.

=item B<check>

Command to check syntax of the Pound configuration file. It will be
called with the filename as argument. Default is B<pound -c -f>.

=head2 [nsupdate DOMAIN]

This section defines parameters needed to send DNS updates for the given
domain. One or more sections (with different DOMAIN tags) can be provided.

The variables in this section are:

=over 4

=item B<ns>

IP address or hostname of the master DNS server for that domain. Multiple
B<ns> records are OK.

=item B<keyfile>

Pathname to the private key file.

=item B<zone>

Domain name for the zone to be updated, if it differs from DOMAIN.

=back

=head1 LICENSE

Copyright (C) 2018 Sergey Poznyakoff

License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

=cut
